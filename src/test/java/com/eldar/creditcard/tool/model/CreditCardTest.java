/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.creditcard.tool.model;

import com.eldar.creditcard.tool.exception.CreditCardExpiratedException;
import com.eldar.creditcard.tool.exception.CreditCardInvalidNumberException;
import com.eldar.creditcard.tool.exception.FormulaException;
import com.eldar.creditcard.tool.exception.InvalidAmountException;
import com.eldar.creditcard.tool.exception.InvalidHolderNameException;
import com.eldar.creditcard.tool.exception.IssuerNotFoundException;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test validations
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class CreditCardTest {

    /**
     * Credit card test
     */
    private final static List<CreditCard> CARDS = new ArrayList<>(10);

    @BeforeAll
    public static void setup() {
        CARDS.add(CreditCardFactory.create("VISA", "4111111111111111", "RAFAEL RODRIGUEZ", "02/26"));
        CARDS.add(CreditCardFactory.create("AMEX", "378282246310005", "ALFREDO ROMERO", "05/23"));
        CARDS.add(CreditCardFactory.create("NARA", "5555555555554444", "JUAN SALAZAR", "01/24"));
        CARDS.add(CreditCardFactory.create("AMEX", "378734493671000", "JULIA M JUAREZ", "05/25"));
        // Duplicated
        CARDS.add(CreditCardFactory.create("VISA", "4111111111111111", "RAFAEL RODRIGUEZ", "02/26"));
    }

    @Test
    public void getFeeOperationThrowsException() {
        assertThrows(InvalidAmountException.class, () -> {
            CARDS.get(0).getFeeOperation(0D);
        });
        assertThrows(InvalidAmountException.class, () -> {
            CARDS.get(0).getFeeOperation(-100D);
        });
        assertThrows(InvalidAmountException.class, () -> {
            CARDS.get(0).getFeeOperation(-1000D);
        });
        assertThrows(InvalidAmountException.class, () -> {
            CARDS.get(0).getFeeOperation(-1000D);
        });
    }

    @Test
    public void validateThrowsException() {
        assertThrows(CreditCardExpiratedException.class, () -> {
            CARDS.get(1).getFeeOperation(100D);
        });
        assertThrows(IssuerNotFoundException.class, () -> {
            CreditCardFactory.create("BAD", "5555555555554444", "JUAN SALAZAR", "01/24");
        });
        assertThrows(FormulaException.class, () -> {
            final CreditCard card = CreditCardFactory.create("VISA", "4111111111111111", "RAFAEL RODRIGUEZ", "02/26");
            card.setFeeFormula("bad formula");
            card.getFeeOperation(100D);
        });
        assertThrows(CreditCardInvalidNumberException.class, () -> {
            CreditCardFactory.create("VISA", "999999", "RAFAEL RODRIGUEZ", "02/26").getFeeOperation(100D);
        });
        assertThrows(InvalidHolderNameException.class, () -> {
            CreditCardFactory.create("NARA", "5555555555554444", "juan antonio p marquez", "02/26").getFeeOperation(100D);
        });
        assertThrows(DateTimeException.class, () -> {
            CreditCardFactory.create("NARA", "5555555555554444", "JUAN P MARQUEZ", "13/26").getFeeOperation(100D);
        });
    }

    @Test
    public void feeOperationNotThrowsException() {
        assertDoesNotThrow(() -> CARDS.get(0).getFeeOperation(125.34D));
        assertDoesNotThrow(() -> CARDS.get(2).getFeeOperation(999.99D));
        assertDoesNotThrow(() -> CARDS.get(3).getFeeOperation(0.1D));
    }

    /**
     * Test if card is usable
     */
    @Test
    public void cardUsable() {
        assertTrue(CARDS.get(0).isUsable());
        assertFalse(CARDS.get(1).isUsable());
        assertTrue(CARDS.get(2).isUsable());
        assertTrue(CARDS.get(3).isUsable());
    }

    @Test
    public void notEqualsCards() {
        assertTrue(CARDS.get(0).notEquals(CARDS.get(1)));
        assertFalse(CARDS.get(0).notEquals(CARDS.get(4)));
    }

    /**
     * Test the issuer fee calculation
     */
    @Test
    public void verifyFeePerIssuer() {
        // Visa: fee = year (yy) / month
        final double feeVisa = Double.valueOf(CARDS.get(0).getExpiration().getYear() % 100) / CARDS.get(0).getExpiration().getMonthValue();
        /* Nara: day * 0.5
         * the day of the month doesn't exist on the credit card expiration 
         * date, value one is assumed.
         */
        final double feeNara = .5D;
        // Amex: month * 0.1
        final double feeAmex = CARDS.get(1).getExpiration().getMonthValue() * .1D;

        // Test Visa
        assertEquals(feeVisa, CARDS.get(0).getFee(), .00000000001D);
        // Test Nara
        assertEquals(feeNara, CARDS.get(2).getFee(), .00000000001D);
        // Test AMEX
        assertEquals(feeAmex, CARDS.get(1).getFee(), .00000000001D);
    }
}
