/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.creditcard.tool;

import com.eldar.creditcard.tool.exception.CreditCardNotFoundException;
import com.eldar.creditcard.tool.model.CreditCard;
import com.eldar.creditcard.tool.model.CreditCardFactory;
import com.eldar.creditcard.tool.model.CreditCardTransact;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Main runner class
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class CreditCardRun {

    /**
     * List of credit card
     */
    private final List<CreditCard> lstCreditCard = new ArrayList<>(100);

    /**
     * List of transactions
     */
    private final List<CreditCardTransact> lstTransact = new ArrayList<>(100);

    /**
     * Self object to run
     */
    private final static CreditCardRun runner = new CreditCardRun();

    /**
     * Main class
     *
     * @param args Arguments
     */
    public static void main(final String[] args) {
        // Print credit card loaded
        runner.printCreditCards();
        // Credit cards:
        runner.printCheckCreditCards();
        // Find one card by number
        runner.findCreditCardByNumber("5427660064241339");
        // Verify transactions
        runner.printVerifyTransactions();
        // Print not equals cards
        runner.printCheckCreditCardNotEquals();
    }

    /**
     * Default constructor to run program
     */
    public CreditCardRun() {
        // Add VISA Cards
        lstCreditCard.add(CreditCardFactory.create("VISA", "4111111111111111", "RAFAEL RODRIGUEZ", "02/26"));
        lstCreditCard.add(CreditCardFactory.create("VISA", "4012888888881881", "MARIA PEREZ", "09/23"));
        lstCreditCard.add(CreditCardFactory.create("VISA", "4222222222222", "ERNESTO RUIZ", "09/23"));
        // Add AMEX Cards
        lstCreditCard.add(CreditCardFactory.create("AMEX", "378282246310005", "ALFREDO ROMERO", "05/23"));
        lstCreditCard.add(CreditCardFactory.create("AMEX", "371449635398431", "MARTA GARCIA", "08/25"));
        lstCreditCard.add(CreditCardFactory.create("AMEX", "378734493671000", "JIMERA RAMIREZ", "10/26"));
        // Add NARA Cards
        lstCreditCard.add(CreditCardFactory.create("NARA", "5555555555554444", "JUAN SALAZAR", "01/24"));
        lstCreditCard.add(CreditCardFactory.create("NARA", "5105105105105100", "ENRIQUE HERNANDEZ", "12/24"));
        lstCreditCard.add(CreditCardFactory.create("NARA", "5323-2834-2893-4027", "JULIA MARQUEZ", "04/26"));
        // Duplicated
        lstCreditCard.add(CreditCardFactory.create("NARA", "5323-2834-2893-4027", "JULIA MARQUEZ", "04/26"));
        lstCreditCard.add(CreditCardFactory.create("NARA", "5427 6600 6424 1339", "julio bimbo salazar", "09/26"));
        // Bad number
        lstCreditCard.add(CreditCardFactory.create("AMEX", "3782 1111 6666 7758", "MARCELO SALAS", "09/25"));

        // Add transactions
        lstTransact.add(new CreditCardTransact("4111111111111111", 38.8D));
        lstTransact.add(new CreditCardTransact("4012888888881881", 1200.9D));
        lstTransact.add(new CreditCardTransact("3782-8224-6310-005", 38.56D));
        lstTransact.add(new CreditCardTransact("5555555555554444", -100.9D));
        lstTransact.add(new CreditCardTransact("5555-5555-5555-4444", 38.8D));
        lstTransact.add(new CreditCardTransact("371449635398431", 600D));
        lstTransact.add(new CreditCardTransact("0000-1111-2222-3333", 600D));
        lstTransact.add(new CreditCardTransact("5321-3215-1624-3948", 540.56D));
        lstTransact.add(new CreditCardTransact("5323-2834-2893-4027", 0D));
        lstTransact.add(new CreditCardTransact("5323-2834-2893-4027", 0D));
        lstTransact.add(new CreditCardTransact("4222222222222", 250D));
        lstTransact.add(new CreditCardTransact("3782111166667758", 250D));
        lstTransact.add(new CreditCardTransact("5427 6600 6424 1339", 150D));
    }

    /**
     * Find a credit card in the list
     *
     * @param cardNumber Card number you can use white space o hyphen as
     * separator
     * @return Credit card found
     * @throws CreditCardNotFoundException when no found
     */
    public CreditCard findCreditCardByNumber(final String cardNumber) {
        return lstCreditCard
                .stream()
                .filter(card -> cardNumber.replaceAll("[-\\s+]", "").equals(card.getCardNumberClean()))
                .findFirst()
                .orElseThrow(() -> new CreditCardNotFoundException(cardNumber + " number not found"));
    }

    /**
     * Return list of credit card
     *
     * @return Collection list
     */
    public List<CreditCard> getCreditCards() {
        return Collections.unmodifiableList(this.lstCreditCard);
    }

    /**
     * Return list of transaction
     *
     * @return Collection list
     */
    public List<CreditCardTransact> getCreditCardTransactions() {
        return Collections.unmodifiableList(this.lstTransact);
    }

    /**
     * Print all credit card
     */
    public void printCreditCards() {
        // Credit cards:
        System.out.println("Printing all credit cards loaded:");
        this.getCreditCards().forEach((CreditCard cc) -> {
            System.out.println(cc.getInfo());
        });
    }

    /**
     * Print credit card information
     *
     * @param cardNumber Card number you can use white space o hyphen as
     * separator
     * @throws CreditCardNotFoundException when no found
     */
    public void printCreditCardInfo(final String cardNumber) {
        System.out.println("\nFind information of Credit Card Number " + cardNumber + ":\n\t" + this.findCreditCardByNumber(cardNumber).getInfo());
    }

    /**
     * Print credit card if is usable or not
     */
    public void printCheckCreditCards() {
        System.out.println("\nChecking credit cards:");
        this.getCreditCards().forEach((CreditCard cc) -> {
            System.out.println(cc.getInfo() + " is usable? " + (cc.isUsable() ? "Yes" : "No"));
        });
    }

    /**
     * Prints if transactions are possible or the issue
     */
    public void printVerifyTransactions() {
        // Verify card transactions
        System.out.println("\nValidate transactions:");
        this.getCreditCardTransactions().forEach((CreditCardTransact cct) -> {
            System.out.print(cct + " is a valid transaction? ");
            try {
                final CreditCard card = this.findCreditCardByNumber(cct.getCardNumber());
                card.getFeeOperation(cct.getAmount());
                System.out.println("yes -> Fee " + card.getIssuerType() + " x Amount (" + card.getFee() + " x " + cct.getAmount() + " = " + card.getFeeOperation(cct.getAmount()) + ")");
            } catch (Exception ex) {
                System.out.println("No -> " + ex.getMessage());
            }
        });
    }

    /**
     * Print examples of credit card comparisons
     */
    public void printCheckCreditCardNotEquals() {
        final int[][] arr = {{0, 4}, {2, 3}, {5, 6}, {8, 9}};
        for (int[] ar : arr) {
            final CreditCard card1 = lstCreditCard.get(ar[0]);
            final CreditCard card2 = lstCreditCard.get(ar[1]);
            System.out.println("CC1 [" + card1.getCardNumber() + "] " + (card1.equals(card2) ? " equals " : " not equals ") + " CC2 [" + card2.getCardNumber() + "]");
        }
    }
}
