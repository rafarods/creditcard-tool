/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.creditcard.tool.model;

import java.time.YearMonth;

/**
 * Nara card
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class CreditCardNara extends CreditCard {

    /**
     * Default values
     */
    public CreditCardNara() {
        this.setIssuerType(IssuerType.NARA);
        this.setFeeFormula("day * 0.5");
        this.setMaxAmount(1000D);
    }

    /**
     * Constructor specialized
     *
     * @param cardNumber Card number
     * @param holderName Holder name
     * @param expiration Expiration month & year
     * @param expFormat Expiration month & year format
     */
    public CreditCardNara(final String cardNumber, final String holderName, final String expiration, final String expFormat) {
        this();
        this.setCardNumber(cardNumber);
        this.setHolderName(holderName);
        this.setExpiration(expiration, expFormat);
    }

    /**
     * Constructor specialized
     *
     * @param cardNumber Card number
     * @param holderName Holder name
     * @param expiration Expiration month & year
     */
    public CreditCardNara(final String cardNumber, final String holderName, final String expiration) {
        this(cardNumber, holderName, expiration, null);
    }

    /**
     * Constructor specialized
     *
     * @param cardNumber Card number
     * @param holderName Holder name
     * @param expiration Expiration month & year
     */
    public CreditCardNara(final String cardNumber, final String holderName, final YearMonth expiration) {
        this();
        this.setCardNumber(cardNumber);
        this.setHolderName(holderName);
        this.setExpiration(expiration);
    }

}
