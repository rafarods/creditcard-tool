/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.creditcard.tool.model;

/**
 * Enumeration type of issuers
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public enum IssuerType {
    /**
     * AMEX type
     */
    AMEX("AMEX"),
    /**
     * VISA type
     */
    VISA("VISA"),
    /**
     * NARA type
     */
    NARA("NARA");

    /**
     * Issuer type ID
     */
    private final String issuerType;

    /**
     * Default constructor
     *
     * @param issuerType Type
     */
    private IssuerType(final String issuerType) {
        this.issuerType = issuerType;
    }

    /**
     * toString method.
     *
     * @return Value of this enumeration as String.
     */
    @Override
    public String toString() {
        return this.issuerType;
    }
}
