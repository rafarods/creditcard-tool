/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.creditcard.tool.model;

import com.eldar.creditcard.tool.exception.InvalidAmountException;
import com.eldar.creditcard.tool.exception.CreditCardExpiratedException;
import com.eldar.creditcard.tool.exception.CreditCardInvalidNumberException;
import com.eldar.creditcard.tool.exception.FormulaException;
import com.eldar.creditcard.tool.exception.InvalidHolderNameException;
import java.time.DateTimeException;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;
import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Base class of card definition and operations
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
@Data
public abstract class CreditCard implements ICreditCard {

    /**
     * Issuer type
     */
    private IssuerType issuerType;

    /**
     * Card number
     */
    private String cardNumber;

    /**
     * Holder name
     */
    private String holderName;

    /**
     * Year & month expiration date. By default, it's expired with next month
     */
    private YearMonth expiration = YearMonth.now().plusMonths(1);

    /**
     * Fee calculation formula
     */
    @EqualsAndHashCode.Exclude
    private String feeFormula;

    /**
     * Max. amount per transaction
     */
    @EqualsAndHashCode.Exclude
    private double maxAmount = 0D;

    /**
     * Regular expression to validate card number
     */
    @EqualsAndHashCode.Exclude
    private String cardNumberRegExpr = "^(?:(?<" + IssuerType.VISA + ">4[0-9]{12}(?:[0-9]{3})?)|"
            + "(?<" + IssuerType.NARA + ">5[1-5][0-9]{14})|"
            + "(?<" + IssuerType.AMEX + ">3[47][0-9]{13}))$";

    /**
     * Regular expression to validate holder name
     */
    @EqualsAndHashCode.Exclude
    private String holderNameRegExpr = "^((?:[A-Z]+ ?){1,3})$";

    /**
     * Get card number without hyphen or spaces
     *
     * @return Card number (clean)
     */
    public String getCardNumberClean() {
        return this.getCardNumber().replaceAll("[-\\s+]", "");
    }

    /**
     * Set expiration card
     *
     * @param expiration Only month and year card expiration
     * @param yearMonthFormat Set null for default format: MM/uu or MM/uuuuu
     * @throws DateTimeException When the expiration is an invalid month and
     * year value
     */
    public void setExpiration(final String expiration, final String yearMonthFormat) {
        final String dtFormat = yearMonthFormat == null || yearMonthFormat.isEmpty()
                ? "MM/[uuuu][uu]"
                : yearMonthFormat;
        this.setExpiration(YearMonth.parse(expiration, DateTimeFormatter.ofPattern(dtFormat)));
    }

    /**
     * Get fee by issuer
     *
     * @return Fee value
     * @throws FormulaException When is a invalid formula
     */
    public double getFee() {
        final ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        final ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("JavaScript");
        final Bindings bindings = new SimpleBindings();
        bindings.put("day", 1);
        bindings.put("month", this.getExpiration().getMonthValue());
        bindings.put("year", this.getExpiration().getYear() % 100);
        try {
            return Double.parseDouble(scriptEngine.eval(this.getFeeFormula(), bindings).toString());
        } catch (ScriptException ex) {
            throw new FormulaException("Failed fee fomula '" + this.getFeeFormula() + "'");
        }
    }

    /**
     * Try to do a fee operation
     *
     * @param amount Amount to operate
     * @return Amount
     */
    public double getFeeOperation(double amount) {
        this.validate(amount);
        return this.getFee() * amount;
    }

    /**
     * Get card information
     *
     * @return Text message
     */
    public String getInfo() {
        StringBuilder sb = new StringBuilder("Issuer='");
        return sb.append(this.getIssuerType()).append("' Card Number='")
                .append(this.getCardNumberClean()).append("' Card Holder='")
                .append(this.getHolderName()).append("' Expiration Date='")
                .append(this.getExpiration().format(DateTimeFormatter.ofPattern("MM/uu")))
                .append("' Fee Formula='")
                .append(this.getFeeFormula()).append("' Fee Value='")
                .append(String.format("%.2f", this.getFee()))
                .append("'").toString();
    }

    /**
     * Is expired card?
     *
     * @return True or false
     */
    public boolean isExpirated() {
        return YearMonth.now().isAfter(this.getExpiration());
    }

    /**
     * Is a valid card number?
     *
     * @return True or false
     */
    public boolean isValidNumber() {
        return Pattern.compile(this.getCardNumberRegExpr())
                .matcher(this.getCardNumberClean()).matches();
    }

    /**
     * Is a valid card holder?
     *
     * @return True or false
     */
    public boolean isValidHolderName() {
        return Pattern.compile(this.getHolderNameRegExpr())
                .matcher(this.getHolderName()).matches();
    }

    /**
     * Validate operation
     *
     * @param amount Amount to validate
     * @throws CreditCardInvalidNumberException
     * @throws InvalidHolderNameException
     * @throws CreditCardExpiratedException
     * @throws InvalidAmountException
     */
    private void validate(final double amount) {
        if (!this.isValidNumber()) {
            throw new CreditCardInvalidNumberException("Invalid card number");
        }
        if (!this.isValidHolderName()) {
            throw new InvalidHolderNameException("Invalid holder name");
        }
        if (this.isExpirated()) {
            throw new CreditCardExpiratedException("Credit card expirated!");
        }
        if (amount < 0) {
            throw new InvalidAmountException("Negative amount is invalid!");
        }
        if (amount == 0) {
            throw new InvalidAmountException("Zero amount is invalid!");
        }
        if (amount >= this.getMaxAmount()) {
            throw new InvalidAmountException("Exceeds maximum amount of " + this.getMaxAmount());
        }
    }

    /**
     * Is not equals to object?
     *
     * @param o Object to be compared
     * @return True or false
     */
    public boolean notEquals(final Object o) {
        return !this.equals(o);
    }

    /**
     * Is an usable credit card?
     *
     * @return True or false
     */
    public boolean isUsable() {
        return !this.isExpirated()
                && this.isValidNumber()
                && this.isValidHolderName()
                && this.getMaxAmount() > 0
                && !this.getFeeFormula().isEmpty();
    }
}
