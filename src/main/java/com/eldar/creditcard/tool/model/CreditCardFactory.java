/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar.creditcard.tool.model;

import com.eldar.creditcard.tool.exception.IssuerNotFoundException;
import static com.eldar.creditcard.tool.model.IssuerType.AMEX;
import static com.eldar.creditcard.tool.model.IssuerType.NARA;
import static com.eldar.creditcard.tool.model.IssuerType.VISA;

/**
 * Factory pattern for credit card creation
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class CreditCardFactory {

    /**
     * Create a credit card
     *
     * @param issuer Issuer name
     * @param cardNumber Credit card number
     * @param holderName Holder name
     * @param expDate Expiration year and month
     * @return Credit card instance
     * @throws IssuerNotFoundException When not found issuer name
     */
    public static CreditCard create(final String issuer, final String cardNumber, final String holderName, final String expDate) {
        if (AMEX.toString().equalsIgnoreCase(issuer)) {
            return new CreditCardAmex(cardNumber, holderName, expDate);
        }
        if (VISA.toString().equalsIgnoreCase(issuer)) {
            return new CreditCardVisa(cardNumber, holderName, expDate);
        }
        if (NARA.toString().equalsIgnoreCase(issuer)) {
            return new CreditCardNara(cardNumber, holderName, expDate);
        }
        throw new IssuerNotFoundException("Issuer type '" + issuer + "' can't found");
    }

}
